#include <iostream>
using namespace std;



double validerDouble(double valeur);
double validerDoublePlusGrandeQue(double valeur, double valeurPlusGrande);

int main() {

	system("pause");
	return 0;
}

double validerDouble(double valeur)
{
	cin >> valeur;
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); 
		cin.ignore(512, '\n');
		cout << "Attention - valeur num�rique : ";
		cin >> valeur;
	}
	cin.ignore(512, '\n');
	return valeur;
}

double validerDoublePlusGrandeQue(double valeur, double valeurPlusGrande)
{
	validerDouble(valeurPlusGrande);
	if (valeurPlusGrande <= valeur)
	{
		cout << "Attention - la valeur doit �tre plus grande que : " << valeur << endl;
		validerDouble(valeurPlusGrande);
	}
	return valeurPlusGrande;
}
