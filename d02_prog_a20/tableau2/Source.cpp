//1) tableau de valeur r�el a 8 case
#include <iostream>
using namespace std;

int main() {
	float  c[8] = { 2, 5, 3, -4, 12, 12, 0, 8 };
	for (int i = 0; i < 8; i++) {
		cout << i << " = " << c[i] << endl;
	}
	system("pause");
	return 0;
}

//2) tableau de valeur r�el a 8 case (contenant 0 au 4 derni�re)
int main2() {
	float  c[8] = { 2, 5, 3, -4 };
	for (int i = 0; i < 8; i++) {
		cout << i << " = " << c[i] << endl;
	}
	system("pause");
	return 0;
}

//3) tableau de valeur enti�re a 12 case (contenant 0 au 8 derni�re)
int main2() {
	int  z[12] = { 0, 0, 8, 0, 0, 6 };
	for (int i = 0; i < 12; i++) {
		cout << i << " = " << z[i] << endl;
	}
	system("pause");
	return 0;
}