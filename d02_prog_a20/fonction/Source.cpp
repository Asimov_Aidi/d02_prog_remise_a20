//#1
int somme(int a, int b) {
	return a + b;
}

//#2
double volume(double longueur, double largeur, double hauteur) {
	return longueur * largeur * hauteur;
}

//#3
int max(int a, int b) {
	int lePlusGrand;
	if (a > b)
	{
		lePlusGrand = a;
	}
	else
	{
		lePlusGrand = b;
	}
	return lePlusGrand;
}