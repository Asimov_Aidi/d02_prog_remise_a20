#include <iostream>
using namespace std;



float moyenneValeursImpaires(int tab[], unsigned short TAILLE_TAB);

int main() {
	const unsigned short TAILLE_TAB = 5;
	int tab[TAILLE_TAB] = { 3,3,2,10,3 };
	cout << moyenneValeursImpaires(tab, TAILLE_TAB);

	system("pause");
	return 0;
}

float moyenneValeursImpaires(int tab[], unsigned short TAILLE_TAB) {
	int	nbValeurImpaire = 0;
	float moyenneValeurImpaire = 0;
	for (int i = 0; i < TAILLE_TAB; i++)
	{
		if (tab[i] % 2 != 0)
		{
			moyenneValeurImpaire = moyenneValeurImpaire + tab[i];
			nbValeurImpaire += 1;
		}
	}
	moyenneValeurImpaire = moyenneValeurImpaire / nbValeurImpaire;
	return moyenneValeurImpaire;
}