#include <iostream>
using namespace std;

//Cr�er une �num�ration
enum Mois { // la syntaxe du nom d'une �num�ration est PascalCase
	JANVIER = 1,
	FEVRIER,
	MARS,
	AVRIL,
	MAI,
	JUIN,
	JUILLET,
	AOUT,
	SEPTEMBRE,
	OCTOBRE,
	NOVEMBRE,
	DECEMBRE
};

enum TypeAvion {
	COMMERCIALE, 
	PRIVE
};


struct avion { 
	
	unsigned int capaciteLitreReservoir;
	unsigned short nombrePlacesAssises;
	unsigned short jourMiseEnService, anneeMiseEnService;
	Mois moisMiseEnService;
	TypeAvion type;
};

int main() {

	const locale LOCALE = locale::global(locale(""));




	unsigned int accumulationPlace = 0;

	char reponse;

	do {


		avion avion; 
	

		unsigned short typeSaisie;
		unsigned short moisMiseEnServiceSaisie;
	

		cout << "Saisir la capacit� du r�servoir : ";
		cin >> avion.capaciteLitreReservoir;

		cout << "Le nombre de places assises : ";
		cin >> avion.nombrePlacesAssises;

		cout << "Le jour de mise en service : ";
		cin >> avion.jourMiseEnService;

		cout << "Le mois de mise en service : ";
		cin >> moisMiseEnServiceSaisie;

		cout << "Le ann�e de mise en service : ";
		cin >> avion.anneeMiseEnService;

		cout << "Le type de l'avion est (0 = commercial / 1 = priv�) : ";
		cin >> typeSaisie;

		accumulationPlace = accumulationPlace + avion.nombrePlacesAssises;

		if (typeSaisie == TypeAvion::COMMERCIALE) {
			avion.type = TypeAvion::COMMERCIALE;
		}
		else if (typeSaisie == TypeAvion::PRIVE) {
			avion.type = TypeAvion::PRIVE;
		}

		
		if (moisMiseEnServiceSaisie == Mois::JANVIER) {

			
			avion.moisMiseEnService = Mois::JANVIER;
		}
		else if (moisMiseEnServiceSaisie == Mois::FEVRIER) {
			avion.moisMiseEnService = Mois::FEVRIER;
		}
		else if (moisMiseEnServiceSaisie == Mois::MARS) {
			avion.moisMiseEnService = Mois::MARS;
		}
		else if (moisMiseEnServiceSaisie == Mois::AVRIL) {
			avion.moisMiseEnService = Mois::AVRIL;
		}
		else if (moisMiseEnServiceSaisie == Mois::MAI) {
			avion.moisMiseEnService = Mois::MAI;
		}
		else if (moisMiseEnServiceSaisie == Mois::JUIN) {
			avion.moisMiseEnService = Mois::JUIN;
		}
		else if (moisMiseEnServiceSaisie == Mois::JUILLET) {
			avion.moisMiseEnService = Mois::JUILLET;
		}
		else if (moisMiseEnServiceSaisie == Mois::AOUT) {
			avion.moisMiseEnService = Mois::AOUT;
		}
		else if (moisMiseEnServiceSaisie == Mois::SEPTEMBRE) {
			avion.moisMiseEnService = Mois::SEPTEMBRE;
		}
		else if (moisMiseEnServiceSaisie == Mois::OCTOBRE) {
			avion.moisMiseEnService = Mois::OCTOBRE;
		}
		else if (moisMiseEnServiceSaisie == Mois::NOVEMBRE) {
			avion.moisMiseEnService = Mois::NOVEMBRE;
		}
		else if (moisMiseEnServiceSaisie == Mois::DECEMBRE) {
			avion.moisMiseEnService = Mois::DECEMBRE;
		}




		cout << "Information de l'avion : " << endl;
		cout << "La capacit� en litre du r�servoir : " << avion.capaciteLitreReservoir << endl;
		cout << "Le nombre de places assises : " << avion.nombrePlacesAssises << endl;
		cout << "La date de mise en service : " << avion.jourMiseEnService << "/" << avion.moisMiseEnService << "/" << avion.anneeMiseEnService << endl;
		cout << "Le type de l'avion : " << avion.type << endl;
		cout << "Voulez-vous saisir un nouvel avion? (o)ui ou (n)on : ";
		cin >> reponse;
	} while (reponse == 'o' || reponse == 'O');

	cout << "Le nombre de places assises total est de : " << accumulationPlace << endl;





	system("pause");
	return 0;
}