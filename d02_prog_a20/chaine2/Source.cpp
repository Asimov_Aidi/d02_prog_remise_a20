#include <iostream>
using namespace std;

int main() {
	char adresse[51];
	
	cout << "Quel est votre adresse: " << endl;
	cin.getline(adresse, 51);
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Adresse trop longue (max 50 caractères), quel est votre adresse : " << endl;
		cin.getline(adresse, 51);
	}
	cout << "Votre adresse est " << adresse << endl;

	system("pause");
	return 0;
}
