#include <iostream>
using namespace std;

const unsigned short TAILLE_NOM = 31;
const unsigned short TAILLE_PRENOM = 31;

enum Position {
	GARDIEN_DE_BUT = 1,
	ATTAQUANT,
	DEFENSEUR,
	MILIEU_DE_TERRAIN
};

struct joueur {
	unsigned short noDeJoueur;
	char nom[TAILLE_NOM];
	char prenom[TAILLE_PRENOM];
	unsigned short nbButs;
	Position position;
};

void validationNumero(joueur joueurs[], unsigned short nbJoueurs, unsigned short &numero);
void validationCaractere(char cara[], const unsigned short TAILLE_NOM);
void validationPosition(short &position);
void validationNumerique(short valeur);
unsigned short afficherMenu();
void ajouterJoueur(joueur joueurs[], unsigned short &nbJoueurs);
int rechercherJoueur(joueur joueurs[], unsigned short nbJoueurs);
void modifierJoueur(joueur &joueur);
void retirerJoueur(joueur joueurs[], unsigned short &nbJoueurs, int indiceJoueurARetirer);
void afficherInfoJoueur(const joueur &JOUEUR);
void ajouterButJoueur(joueur &joueur);
void retirerButJoueur(joueur &joueur);
void afficherJoueursParPosition(joueur joueurs[], unsigned short nbJoueurs);
void afficherTotalDeButDeLEquipe(joueur joueurs[], unsigned short nbJoueurs);

int main() {
	const locale LOCALE = locale::global(locale(""));
	unsigned short choixMenu;
	joueur equipe[14];
	unsigned short nbJoueurs = 0;

	do
	{
		choixMenu = afficherMenu();
		if (choixMenu == 1)
		{
			ajouterJoueur(equipe, nbJoueurs);
			system("pause");
		}
		else if (choixMenu == 2)
		{
			int indiceJoueurAModifier;
			indiceJoueurAModifier = rechercherJoueur(equipe, nbJoueurs);
			if (indiceJoueurAModifier != -1)
			{
				modifierJoueur(equipe[indiceJoueurAModifier]);
				unsigned short numero = equipe[indiceJoueurAModifier].noDeJoueur;
				validationNumero(equipe, nbJoueurs, numero);
				equipe[indiceJoueurAModifier].noDeJoueur = numero;
			}
			
			system("pause");
		}
		else if (choixMenu == 3)
		{
			int indiceJoueurAEnlever;
			indiceJoueurAEnlever = rechercherJoueur(equipe, nbJoueurs);
			if (indiceJoueurAEnlever != -1)
			{
				retirerJoueur(equipe, nbJoueurs, indiceJoueurAEnlever);
			}
			
			system("pause");
		}
		else if (choixMenu == 4)
		{
			int indiceJoueurAAfficher;
			indiceJoueurAAfficher = rechercherJoueur(equipe, nbJoueurs);
			if (indiceJoueurAAfficher != -1)
			{
				afficherInfoJoueur(equipe[indiceJoueurAAfficher]);
			}
			
			system("pause");
		}
		else if (choixMenu == 5)
		{
			int indiceJoueur;
			indiceJoueur = rechercherJoueur(equipe, nbJoueurs);
			if (indiceJoueur != -1)
			{
				ajouterButJoueur(equipe[indiceJoueur]);
			}
			
			system("pause");
		}
		else if (choixMenu == 6)
		{
			int indiceJoueur;
			indiceJoueur = rechercherJoueur(equipe, nbJoueurs);
			if (indiceJoueur != -1)
			{
				retirerButJoueur(equipe[indiceJoueur]);
			}
			
			system("pause");
		}
		else if (choixMenu == 7)
		{
			afficherJoueursParPosition(equipe, nbJoueurs);
			system("pause");
		}
		else if (choixMenu == 8)
		{
			afficherTotalDeButDeLEquipe(equipe, nbJoueurs);
			system("pause");
		}
	} while (choixMenu != 9);
	return 0;
}

/*
	T�che : fonction qui fait apparaitre un menu, demande de faire un
			choix � l'utilisateur, r�cup�re ce choix et le retourne.
	Param�tre(s) : aucun
	Retour : Le choix effectu� par l'utilisateur
*/
unsigned short afficherMenu() {
	
	unsigned short choixMenu;
	cout << "Menu" << endl;
	cout << "1 - Ajouter un joueur dans l'�quipe" << endl;
	cout << "2 - modifier un joueur dans l'�quipe" << endl;
	cout << "3 - retirer un joueur de l'�quipe" << endl;
	cout << "4 - afficher les informations d'un joueur selon son num�ro" << endl;
	cout << "5 - ajouter un but � un joueur" << endl;
	cout << "6 - enlever un but � un joueur" << endl;
	cout << "7 - Afficher tous les joueurs (avec option de filtre)" << endl;
	cout << "8 - Afficher le nombre total de buts compt�s pour l��quipe" << endl;
	cout << "9 - Quitter" << endl;
	cout << "Votre choix : ";
	cin >> choixMenu;
	validationNumerique(choixMenu);
	cin.ignore(512, '\n');

	return choixMenu; 
}

/*
	T�che : fonction qui sert � ins�rer UN SEUL nouveau joueur dans l'�quipe. Demander une saisie pour chacune des informations d�un joueur, sauf pour le nombre de but, qui sera initialis� � 0 au moment de la cr�ation.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
	Retour : aucun (sortie via les param�tres formels pass�s par adresse et/ou r�f�rence)
*/
void ajouterJoueur(joueur joueurs[], unsigned short &nbJoueurs) {
	
	joueur joueurAAjouter;
	cout << "Saisir les informations du joueur � ajouter : " << endl;
	cout << "Pr�nom du joueur : ";
	cin.getline(joueurAAjouter.prenom, TAILLE_PRENOM);
	validationCaractere(joueurAAjouter.prenom, TAILLE_NOM);

	cout << "Nom du joueur : ";
	cin.getline(joueurAAjouter.nom, TAILLE_NOM);
	validationCaractere(joueurAAjouter.nom, TAILLE_NOM);
	
	unsigned short numero;
	cout << "Le num�ro du joueur : ";
	cin >> numero;
	validationNumerique(numero);
	
	validationNumero(joueurs, nbJoueurs, numero);
	joueurAAjouter.noDeJoueur = numero;

	joueurAAjouter.nbButs = 0;

	short typeSaisie;
	cout << "Position du joueur (1=gardien, 2=attaquant, 3=defenseur, 4=millieu): ";
	cin >> typeSaisie;
	validationNumerique(typeSaisie);
	validationPosition(typeSaisie);
	joueurAAjouter.position = (Position)typeSaisie;

	if (nbJoueurs < 14)
	{
		joueurs[nbJoueurs] = joueurAAjouter;
		nbJoueurs++;
		cout << "Le joueur a �t� ajout� avec succes" << endl;
	}
	else
	{
		cout << "�quipe d�ja pleine " << endl;
	}
}

/*
	T�che : fonction qui demande la saisie d'un num�ro de joueur et
			cherche dans la liste des joueurs de l'�quipe dans le
			but de retrouver ce joueur. Si le num�ro du joueur saisi
			correspond au num�ro d'un des joueurs de l'�quipe, la
			fonction retourne l'indice ou se trouve le joueur dans
			le tableau, sinon la fonction affiche le message
			"Ce joueur n'existe pas" et retourne la valeur -1.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
*/
int rechercherJoueur(joueur joueurs[], unsigned short nbJoueurs) {
	
	int	indice = -1;
	int indiceCourant = 0;
	int valeurRechercher;
	cout << "Quel est le num�ro du joueur rechercher : " << endl;
	cin >> valeurRechercher;
	validationNumerique(valeurRechercher);

	while (indiceCourant < nbJoueurs && indice == -1) {
	
		if (valeurRechercher == joueurs[indiceCourant].noDeJoueur) {
			indice = indiceCourant;
		}
		else {
			indiceCourant++;
		}
	}
	if (indice == -1)
	{
		cout << "Ce joueur n'existe pas" << endl;
	}
	return indice;
}


/*
	T�che : fonction qui permet de modifier les informations D'UN SEUL joueur
		 pass� en param�tre. Les seules informations que vous pouvez modifier
		 pour un joueur sont le num�ro du joueur, le nom, le pr�nom et la
		 position. Vous ne devez pas permettre de modifier le nombre de buts.
	Param�tre : une r�f�rence d'un joueur.
*/
void modifierJoueur(joueur &joueur) {
	
	afficherInfoJoueur(joueur);
	cout << "Saisir les informations du joueur � modifier : " << endl;
	cout << "Pr�nom du joueur : ";
	cin.getline(joueur.prenom, TAILLE_PRENOM);
	validationCaractere(joueur.prenom, TAILLE_NOM);

	cout << "Nom du joueur : ";
	cin.getline(joueur.nom, TAILLE_NOM);
	validationCaractere(joueur.nom, TAILLE_NOM);

	short typeSaisie;
	cout << "Position du joueur (1=gardien, 2=attaquant, 3=defenseur, 4=millieu): ";
	cin >> typeSaisie;
	validationNumerique(typeSaisie);
	validationPosition(typeSaisie);
	joueur.position = (Position)typeSaisie;

	cout << "Le num�ro du joueur : ";
	cin >> joueur.noDeJoueur;
	validationNumerique(joueur.noDeJoueur);
	
}


/*
	T�che : fonction qui retire un joueur du tableau en fonction
			de l'indice o� se trouve le joueur dans le tableau.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
				l'indice o� se trouve joueur dans le tableau
	Retour : aucun (sortie via les param�tres formels pass�s par
			 adresse et/ou r�f�rence)
*/
void retirerJoueur(joueur joueurs[], unsigned short &nbJoueurs, int indiceJoueurARetirer) {
	
	if (nbJoueurs == indiceJoueurARetirer)
	{
		joueurs[indiceJoueurARetirer].nbButs = 0;
		joueurs[indiceJoueurARetirer].noDeJoueur = 0;
		joueurs[indiceJoueurARetirer].position = (Position)0;
		for (int i = 0; i < TAILLE_NOM; i++)
		{
			joueurs[indiceJoueurARetirer].nom[i] = (char)0;
			joueurs[indiceJoueurARetirer].prenom[i] = (char)0;
		}
		nbJoueurs--;
	}
	else
	{
		joueurs[indiceJoueurARetirer].nbButs = joueurs[nbJoueurs].nbButs;
		joueurs[indiceJoueurARetirer].noDeJoueur = joueurs[nbJoueurs].noDeJoueur;
		joueurs[indiceJoueurARetirer].position = joueurs[nbJoueurs].position;
		for (int i = 0; i < TAILLE_NOM; i++)
		{
			joueurs[indiceJoueurARetirer].nom[i] = joueurs[nbJoueurs].nom[i];
			joueurs[indiceJoueurARetirer].prenom[i] = joueurs[nbJoueurs].prenom[i];
		}
		
		nbJoueurs--;
	}
}


/*
	T�che : fonction qui affiche les informations D'UN SEUL joueur
			pass� en param�tre.
	Param�tre : une r�f�rence constante d'un joueur pour �viter tout
				risque de modification du joueur dans la fonction.
	Retour : aucun (affichage seulement)
*/
void afficherInfoJoueur(const joueur &JOUEUR) {
	
	cout << "Les informations du joueur sont : " << endl;
	cout << "Le num�ro du joueur est " << JOUEUR.noDeJoueur << endl;
	cout << "Le pr�nom du joueur est " << JOUEUR.prenom << endl;
	cout << "Le nom du joueur est " << JOUEUR.nom << endl;
	cout << "La position du joueur est " << JOUEUR.position << endl;
	cout << "Le nombre de but du joueur est " << JOUEUR.nbButs << endl;
	cout << "********************************** " << endl;
}

/*
	T�che : fonction qui ajoute un but � un joueur pass� par r�f�rence
			en param�tre.
	Param�tre : une r�f�rence d'un joueur
	Retour : aucun (sortie via les param�tres formels pass�s par
			 adresse et/ou r�f�rence)
*/
void ajouterButJoueur(joueur &joueur) {
	//� compl�ter
	joueur.nbButs += 1;
	cout << "Le but a �t� ajout�" << endl;
}

/*
	T�che : fonction qui retire un but � un joueur pass� par r�f�rence
			en param�tre. La fonction doit pr�voir que le nombre de but
			ne pourra pas jamais �tre inf�rieur � 0.
	Param�tre : une r�f�rence d'un joueur
	Retour : aucun (sortie via les param�tres formels pass�s par
			 adresse et/ou r�f�rence)
*/
void retirerButJoueur(joueur &joueur) {
	
	if (joueur.nbButs > 0)
	{
		joueur.nbButs -= 1;
		cout << "Le but a �t� enlev�" << endl;
	}
	else
	{
		joueur.nbButs = 0;
		cout << "Le nombre de but est d�ja � 0" << endl;
	}
	
}


/*
	T�che : fonction qui affiche les joueurs de l'�quipe apr�s avoir
			demand� la saisie de la position � afficher. Aussi, ajouter
			une option permettant d'afficher tous les joueurs.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
	Retour : aucun (affichage seulement)
*/
void afficherJoueursParPosition(joueur joueurs[], unsigned short nbJoueurs) {
	
	short positionRechercher;
	cout << "Les joueurs de quelle position voulez-vous afficher? (0=tous, 1=gardien, 2=attaquant, 3=defenseur, 4=millieu)" << endl;
	cin >> positionRechercher;
	validationNumerique(positionRechercher);
	while (positionRechercher > 4 || positionRechercher < 0)
	{
		cout << "position invalide " << endl;
		cin >> positionRechercher;
		validationNumerique(positionRechercher);
	}
	
	if (positionRechercher != 0)
	{
		for (int i = 0; i < nbJoueurs; i++)
		{
			if (joueurs[i].position == positionRechercher)
			{
				afficherInfoJoueur(joueurs[i]);
			}
		}
	}
	else
	{
		for (int i = 0; i < nbJoueurs; i++)
		{
			afficherInfoJoueur(joueurs[i]);
		}
	}
}



/*
	T�che : fonction qui calcule et affiche le nombre total de but(s)
			marqu�(s) pour toutes l'�quipes.
	Param�tre : un tableau de variable de type struct joueur
				le nombre de joueurs actuellement dans l'�quipe
	Retour : aucun (affichage seulement)
*/
void afficherTotalDeButDeLEquipe(joueur joueurs[], unsigned short nbJoueurs) {
	
	int totalBut = 0;
	for (int i = 0; i < nbJoueurs; i++)
	{
		totalBut = totalBut + joueurs[i].nbButs ;
	}
	cout << "Le total des buts de l'�quipe est de : " << totalBut << endl;
}

void validationNumerique(short valeur) {
	while (cin.fail() || cin.peek() != '\n') {
		cin.clear(); 
		cin.ignore(512, '\n');
		cout << "Attention - valeur num�rique : ";
		cin >> valeur;
	}
	cin.ignore(512, '\n');
}

void validationCaractere(char cara[], const unsigned short TAILLE_NOM) {
	while (cin.fail()) {
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Attention - entr�e non-valide : ";
		cin.getline(cara, TAILLE_NOM);

	}
	cin.ignore(512, '\n');
}

void validationPosition(short &position)
{
	if (position > 4 || position < 1)
	{
		cout << "position invalide" << endl;
		cin >> position;
		validationNumerique(position);
		validationPosition(position);
	}
}

void validationNumero(joueur joueurs[], unsigned short nbJoueurs, unsigned short &numero) {
	
	int	indice = -1;
	while (numero < 1 || numero > 100)
	{
		cout << "Ce num�ro n'est pas entre 1 et 100" << endl;
		cin >> numero;
		validationNumerique(numero);
	}
	do
	{
		int	indice = -1;
		int indiceCourant = 0;
	
		while (indiceCourant < nbJoueurs && indice == -1) {

			if (numero == joueurs[indiceCourant].noDeJoueur) {
				indice = indiceCourant;
			}
			else {
				indiceCourant++;
			}
		}
		if (indiceCourant == nbJoueurs)
		{
			indice = -1;
		}
		if (indice != -1) {
			cout << "Ce num�ro est d�j� associer a un joueur" << endl;
			cout << "Quel est le num�ro du joueur" << endl;
			cin >> numero;
			validationNumerique(numero);
			validationNumero(joueurs, nbJoueurs, numero);
		}
	} while (indice != -1);
	
}


