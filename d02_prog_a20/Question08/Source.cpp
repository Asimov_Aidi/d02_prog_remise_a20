#include <iostream>
using namespace std;

struct produit
{
	double prix;
	unsigned short quantite;
};

double calculerSousTotal(double prix, unsigned short quantite);
double calculerTaxe(double sousTotal, double pourcentageTaxe);
void afficher(produit &produit);

int main() {
	produit produit;
	cout << "Quel est le prix du produit? : " << endl;
	cin >> produit.prix;

	cout << "Quel est la quantite du produit? : " << endl;
	cin >> produit.quantite;

	afficher(produit);

	system("pause");
	return 0;
}

double calculerSousTotal(double prix, unsigned short quantite)
{
	double sousTotal;
	sousTotal = prix * quantite;

	return sousTotal;
}

double calculerTaxe(double sousTotal, double pourcentageTaxe) {
	double taxeAppliquer;
	taxeAppliquer = sousTotal * pourcentageTaxe;

	return taxeAppliquer;
}

void afficher(produit & produit)
{
	double taxe = 0.15;
	cout << "Le prix du produit est de : " << produit.prix << endl;
	cout << "La quantite du produit est de : " << produit.quantite << endl;
	cout << "Le sous-total est de : " << calculerSousTotal(produit.prix, produit.quantite) << endl;
	cout << "Le montant de la taxe est de : " << calculerTaxe(calculerSousTotal(produit.prix, produit.quantite), taxe) << endl;
	cout << "Le grand total est de : " << calculerSousTotal(produit.prix, produit.quantite) + calculerTaxe(calculerSousTotal(produit.prix, produit.quantite), taxe) << endl;

}
