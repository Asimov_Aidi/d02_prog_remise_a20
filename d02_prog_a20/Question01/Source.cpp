#include <iostream>
using namespace std;



double montantImpotAPayer(double salaireBrute);


int main() {

	cout << montantImpotAPayer(40000);

	system("pause");
	return 0;
}

double montantImpotAPayer(double salaireBrute)
{
	double impotAPayer;
	if (salaireBrute < 20000)
	{
		impotAPayer = (salaireBrute / 100) * 20;
	}
	else if (salaireBrute >= 20000 && salaireBrute < 45000)
	{
		impotAPayer = (salaireBrute / 100) * 25;
	}
	else if (salaireBrute >= 45000)
	{
		impotAPayer = (salaireBrute / 100) * 30;
	}

	return impotAPayer;
}
