/*
DEBUT
	i = 2
	Premier = 1
	LIRE Nombre
	Si Nombre >= 0 ET Nombre <= 100 ALORS
		TANTQUE Premier = 1 ET i < Nombre FAIRE
			SI Nombre % i = 0 FAIRE
				Premier = 0
			SINON
				Premier = 1
			FINSI
			i = i +1
		FINTANTQUE
		SI Premier = 0
			ECRIRE "Votre nombre n'est pas premier"
		SINON
			ECRIRE "Votre Nombre est premier"
		FINSI
	Sinon
		ECRIRE "Veuillez saisir une entr�e entre 0 et 100: "
	FINSI
FIN
*/

#include <iostream>
using namespace std;

int main() {
	int nombre;
	int i;
	bool premier;
	int resultat;
	i = 2;
	premier = 1;

	cin >> nombre;
	if (nombre >= 0 && nombre <= 100)
	{
		while (premier == 1 && i < nombre)
		{
			resultat = nombre % i;
			if (resultat == 0)
			{
				premier = 0;
			}
			else 
			{
				premier = 1;
			}
			i = i + 1;
		}
		if (premier == 0)
		{
			cout << "Votre nombre n'est pas premier" << endl;
		}
		else 
		{
			cout << "Votre nombre est premier" << endl;
		}
	}
	else
	{
		cout << "Votre entr�e n'est pas valide" << endl;
	}


	system("pause");
	return 0;
}
