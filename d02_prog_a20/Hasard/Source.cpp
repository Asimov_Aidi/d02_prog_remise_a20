#include <iostream>
#include <time.h>
using namespace std;


void genererValeurAleatoire(short min, short max, short &nombreGenerer);
void validerNumerique(short &valeurNumerique);
void validerEntreMinEtMax(short &valeur);
int determinerSiTrouver(int nombreGenere, int valeur);
void montantGain(int nombreTentative, double &gain);
void ordinateurQuiJoue();


int main() {
	srand(time(0));
	short valeurATrouver;
	short valeurSaisie;
	int resultat;
	double montantGagner;
	int nombreTentative = 0;
	ordinateurQuiJoue();
	cout << "Veuillez saisir un chiffre" << endl;
	//g�n�rer nombre
	genererValeurAleatoire(1, 100, valeurATrouver);
	do {
		//validation
		validerEntreMinEtMax(valeurSaisie);
		//saisie = g�n�rer?
		resultat = determinerSiTrouver(valeurATrouver, valeurSaisie);
		if (resultat == -1)
		{
			cout << "Votre nombre est trop petit" << endl;
		}
		else if (resultat == 1)
		{
			cout << "Votre nombre est trop grand" << endl;
		}
		else if (resultat == 0)
		{
			cout << "Nombre trouv�!" << endl;
		}
		nombreTentative += 1;
	} while (resultat != 0);

	//gain
	montantGain(nombreTentative, montantGagner);
	cout << "Le montant gagn� est de : " << montantGagner << endl;

	system("pause");
	return 0;
}

void ordinateurQuiJoue() {
	srand(time(0));
	short valeurATrouver;
	short valeurSaisie;
	int resultat;
	double montantGagner;
	int nombreTentative = 0;
	int min = 1;
	int max = 100;
	
	validerEntreMinEtMax(valeurSaisie);
	do {
		genererValeurAleatoire(min, max, valeurATrouver);
		cout << valeurATrouver << endl;
		resultat = determinerSiTrouver(valeurATrouver, valeurSaisie);
		if (resultat == 1)
		{
			cout << "Votre nombre est trop petit" << endl;
			min = valeurATrouver;
		}
		else if (resultat == -1)
		{
			cout << "Votre nombre est trop grand" << endl;
			max = valeurATrouver;
		}
		else if (resultat == 0)
		{
			cout << "Nombre trouv�!" << endl;
		}
		nombreTentative += 1;
	} while (resultat != 0);
	cout << "L'ordinateur � pris : " << nombreTentative << " tentative pour gagner " << endl;
	system("pause");
}

void genererValeurAleatoire(short min, short max, short &nombreGenerer) {
	nombreGenerer = (rand() % (max - min + 1)) + min;
}

void validerNumerique(short &valeurNumerique) {
	cin >> valeurNumerique;
	while (cin.fail() || cin.peek() != '\n'){
		cin.clear();
		cin.ignore(512, '\n');
		cout << "Valeur num�rique seulement"<<endl;
		cin >> valeurNumerique;
	}
}

void validerEntreMinEtMax(short &valeur) {
	validerNumerique(valeur);
	while (valeur < 0 || valeur > 100){
		cout << "valeur n'est pas entre 0 et 100" << endl;
		validerNumerique(valeur);
	}
}

int determinerSiTrouver(int nombreGenere, int valeur) {
	int resultat;
	if (nombreGenere == valeur){
		resultat = 0;
	}
	else if (nombreGenere > valeur){
		resultat = -1;
	}
	else if (nombreGenere < valeur) {
		resultat = 1;
	}
	return resultat;
}

void montantGain(int nombreTentative, double &gain) {
	switch (nombreTentative){
	case 1:
		gain = 100;
		break;
	case 2:
		gain = 90;
		break;
	case 3:
		gain = 80;
		break;
	case 4:
		gain = 70;
		break;
	case 5:
		gain = 60;
		break;
	case 6:
		gain = 50;
		break;
	case 7:
		gain = 40;
		break;
	case 8:
		gain = 30;
		break;
	case 9:
		gain = 20;
		break;
	case 10:
		gain = 10;
		break;
	default:
		gain = 0;
		break;
	}
}

