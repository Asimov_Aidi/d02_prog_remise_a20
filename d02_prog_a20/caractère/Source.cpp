#include <iostream>
using namespace std;

int longueur(char tab[], const int TAILLE);
void ALEnvers(char tab[], const int TAILLE);
bool palindrome(char tab[], const int TAILLE);

int main() {
	char nom[] = "ressasser";
	cout << palindrome(nom, 9) << endl;

	system("pause");
	return 0;
}



int longueur(char tab[], const int TAILLE) {
	int longueurChaine = 0;
	while (tab[longueurChaine] != 0) { 
		longueurChaine++;
	}
	cout << "La longueur de la chaine de caract�re est " << longueurChaine << endl;
	return longueurChaine;
}

void ALEnvers(char tab[], const int TAILLE) {
	int	PremierCaractere1;
	PremierCaractere1 = longueur(tab,TAILLE) - 1;
	for (int k = PremierCaractere1; k >= 0; k--)
	{
		cout << tab[k];
	}
	cout << endl;
}

bool palindrome(char tab[], const int TAILLE) {
	
	
	bool identique = 1;
	int j = 0;
	int i = TAILLE - 1;
	while (identique == 1 && j < TAILLE/2)
	{
		if (tab[j] == tab[i])
		{
			identique = 1;
		}
		else
		{
			identique = 0;
		}
		i--;
		j++;
	}
	return identique;
}