#include <iostream>
using namespace std;


int main() {
	int resultat1;
	resultat1 = 20 / 3;
	cout << "Le r�sultat est de : " << resultat1 << endl;
	//ne prend pas en compte les d�cimals

	double resultat2;
	resultat2 = 20 / 3;
	cout << "Le r�sultat est de : " << resultat2 << endl;
	//prend pas en compte les d�cimals 

	double resultat3;
	resultat3 = (double)20 / 3;
	cout << "Le r�sultat est de : " << resultat3 << endl;
	//prend en compte les d�cimals

	double resultat4;
	resultat4 = (double)(20 / 3);
	cout << "Le r�sultat est de : " << resultat4 << endl;
	//prend pas en compte les d�cimals (op�ration avant casting)

	system("pause");
	return 0;
}
