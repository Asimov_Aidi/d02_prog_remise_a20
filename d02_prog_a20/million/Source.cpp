/*
Pseudo-code a
VARIABLES
	Investissement : R�el
	�pargne : R�el
CONSTANTES
	Temps = 5 : R�el
	Inter�t = 0,10 : R�el
D�BUT
	Investissement = 500 000
	POUR i = 1 � 5
		Investissement = Investissement * (1 + Inter�t)
	FINPOUR
	�pargne = Investissement
	ECRIRE "Votre �pargne apr�s 5 ans est de: ", �pargne, "$"
FIN
*/
#include <iostream>
using namespace std;

int main() {
	double Investissement;
	double Epargne;
	const double Temps = 5;
	const double Interet = 0.10;

	Investissement = 500000;
	for (int i = 1; i < 5; i++)
	{
		Investissement = Investissement * (1 + Interet);
	}
		
	Epargne = Investissement;
	cout << "Votre �pargne apr�s 5 ans est de: " << Epargne << endl;

	system("pause");
	return 0;
}