/*
	ECRIRE "2"
	POUR i = 3 � 50000
		Premier = 1
		Compteur = 2
		TANTQUE Premier = 1 ET Compteur < i FAIRE
			SI i % Compteur = 0 FAIRE
				Premier = 0
			SINON
				Premier = 1
			FINSI
			Compteur = Compteur +1
		FINTANTQUE
		SI Premier = 1
			ECRIRE i
		FINSI
	FINPOUR
*/
#include <iostream>
using namespace std;

int main() {
	bool Premier;
	int Compteur;
	int Resultat;

	cout << '2';
	for (int i = 3; i < 50000; i++)
	{
		Premier = 1;
		Compteur = 2;
		while (Premier == 1 && Compteur < i)
		{
			Resultat = i % Compteur;
			if (Resultat == 0) {
				Premier = 0;
			}
			else
			{
				Premier = 1;
			}
			Compteur += 1;
		}
		if (Premier == 1)
		{
			cout << i << endl;
		}
	}

	system("pause");
	return 0;
}