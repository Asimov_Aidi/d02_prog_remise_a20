/* 
Analyse
Entr�
	Note : R�el
Sortie
	NoteGlobal
Donn�es interne
	NoteGrande
	NotePetite
Traitement
	Demander en boucle la saisie d'une note et sauvegarder la somme
	SI note > NoteGrande
		NoteGrande = Note
	SI NotePetite > Note ALORS
		NotePetite = Note
	Soustraire la note grande et petit de la somme puis diviser par 4
	donne la note global

Pseudo code
Variables
	Note : R�el
	NoteGlobal : R�el
	NoteGrande : R�el
	NotePetite : R�el
DEBUT
	LIRE Note
	NoteGrande = Note
	NotePetite = Note
	NoteGlobal = Note
	POUR i = 1 � 6 FAIRE
		LIRE Note
		SI NoteGrande < Note ALORS
			NoteGrande = Note
		FINSI
		SI NotePetite > Note ALORS 
			NotePetite = Note
		FINSI
		NoteGlobal = NoteGlobal + Note
		
	FINPOUR
	NoteGlobal = NoteGlobal - NotePetite - NoteGrande
	NoteGlobal = NoteGlobal / 4
FIN
*/

#include <iostream>
using namespace std;

int main() {
	double Note;
	double NoteGlobal;
	double NoteGrande;
	double NotePetite;

	cout << "Saisir la note: ";
	cin >> Note;
	NoteGrande = Note;
	NotePetite = Note;
	NoteGlobal = Note;
	for (int i = 1; i < 6; i++)
	{
		cout << "Saisir la note: ";
		cin >> Note;
		if (NoteGrande < Note){
			NoteGrande = Note;
		};
		if (NotePetite > Note){
			NotePetite = Note;
		}
		NoteGlobal = NoteGlobal + Note;

	}
	NoteGlobal = NoteGlobal - NoteGrande - NotePetite;
	NoteGlobal = NoteGlobal / 4;
	cout << "La Note global est:" << NoteGlobal;


	system("pause");
	return 0;
}