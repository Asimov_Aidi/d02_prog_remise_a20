/* Pseudo code
Variables
	NombreNote : Entier
	NoteSaisie : R�el
	MoyenneNote : R�el
D�but
	LIRE NombreNote
	POUR I = 0 � NombreNote FAIRE
	LIRE NoteSaisie
	MoyenneNote = (MoyenneNote + NoteSaisie) / I
	FINPOUR
	ECRIRE MoyenneNote
FIN
*/

#include <iostream>
using namespace std;

int main() {
	int NombreNote;
	double NoteSaisie;
	double MoyenneNote;
	cout << "Nombre de note � saisir: " << endl;
	MoyenneNote = 0;
	cin >>  NombreNote;
	for (int i = 1; i < (NombreNote + 1); i++)
	{
		cout << "La note saisie est: " << endl;
		cin >> NoteSaisie;
		while (NoteSaisie < 0 || NoteSaisie > 100)
		{
			cout << "Entre o et 100 seulement: " << endl;
			cin >> NoteSaisie;
		}
		MoyenneNote = (MoyenneNote + NoteSaisie);
			
	}
	MoyenneNote = MoyenneNote / NombreNote;
	cout << "La moyenne du groupe est: " << MoyenneNote << endl;
	
	system("pause");
	return 0;
}