/*
Pseudo-code
Variables
	Nombre : R�el
DEBUT
	REPETER
		LIRE Nombre
	TANTQUE Nombre < 0 OU Nombre > 100
FIN
*/
#include <iostream>
using namespace std;

int main() {
	double nombre;

	do
	{
		cin >> nombre;
	} while (nombre < 0 || nombre > 100);
	
	
	system("pause");
	return 0;
}