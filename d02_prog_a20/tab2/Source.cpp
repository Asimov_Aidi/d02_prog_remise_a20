#include <iostream>
#include <math.h>
using namespace std;

void initialiser(int tab[], int TAILLE);

int main() {
	const int TAILLE = 20;
	int tabPremier[TAILLE];
	int tabDernier[TAILLE];
	int tabProduit[TAILLE];
	int somme = 0;

	initialiser(tabPremier, TAILLE);
	initialiser(tabDernier, TAILLE);
	for (int i = 0; i < TAILLE; i++)
	{
		tabProduit[i] = tabPremier[i] * tabDernier[i];
		cout << tabPremier[i] << " | " << tabDernier[i] <<" | " << tabProduit[i] << endl;
	}
	for (int j = 0;  j < TAILLE; j++)
	{
		somme = somme + tabProduit[j];
	}
	cout << "Racine carr� de la somme du tableau produit " << sqrt(somme) << endl;

	system("pause");
	return 0;
}

void initialiser(int tab[], int TAILLE) {
	for (int i = 0; i < TAILLE; i++) {
		cout << "Valeur de l'indice " << i << endl;
		cin >> tab[i];
	}
}