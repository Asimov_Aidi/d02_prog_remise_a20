#include <iostream>
using namespace std;

//Cr�er une �num�ration
enum Mois { // la syntaxe du nom d'une �num�ration est PascalCase
	JANVIER = 1,
	FEVRIER,
	MARS,
	AVRIL,
	MAI,
	JUIN,
	JUILLET,
	AOUT,
	SEPTEMBRE,
	OCTOBRE,
	NOVEMBRE,
	DECEMBRE
};



struct avion { // la syntaxe du nom d'une struct est camelCase
	//On peut placer dans une struct des d�clarations de variables qui la compose
	unsigned int capaciteLitreReservoir;
	unsigned short nombrePlacesAssises;
	unsigned short jourMiseEnService, anneeMiseEnService;
	Mois moisMiseEnService;
};

int main() {

	const locale LOCALE = locale::global(locale(""));

	

	//nom du pilote de l'avion avec la plus grande capacit�
	/*
	unsigned int capaciteLitreReservoirAvionPlusGrandeCapacite;
	unsigned short nombrePlacesAssisesAvionPlusGrandeCapacite;
	unsigned short jourMiseEnServiceAvionPlusGrandeCapacite, anneeMiseEnServiceAvionPlusGrandeCapacite;
	Mois moisMiseEnServiceAvionPlusGrandeCapacite; // cr�e une VARIABLE de type d'�num�ration Mois nomm�e moisMiseEnService
	*/

	unsigned int accumulationPlace = 0;

	char reponse;

	do {


		avion avion; // d�claration d'une variable de type avion nomm�e avion
		/*
		avion.capaciteLitreReservoir = 25; // exemple d'assignation de valeur � une variable emmagasin� dans ma variable de type avion
		avion.moisMiseEnService = Mois::AOUT;
		*/

		//nom du pilote


		unsigned short moisMiseEnServiceSaisie;
		//Mois moisMiseEnService; // cr�e une VARIABLE de type d'�num�ration Mois nomm�e moisMiseEnService

		cout << "Saisir la capacit� du r�servoir : ";
		cin >> avion.capaciteLitreReservoir;

		cout << "Le nombre de places assises : ";
		cin >> avion.nombrePlacesAssises;

		cout << "Le jour de mise en service : ";
		cin >> avion.jourMiseEnService;

		cout << "Le mois de mise en service : ";
		cin >> moisMiseEnServiceSaisie;

		cout << "Le ann�e de mise en service : ";
		cin >> avion.anneeMiseEnService;

		accumulationPlace = accumulationPlace + avion.nombrePlacesAssises;

		//J'utilise les valeurs de mon �num�ration Mois pour les comparer avec la saisie de l'utilisateur
		if (moisMiseEnServiceSaisie == Mois::JANVIER) {

			//Je place dans ma variable de type Mois nomm�e moisMiseEnService un valeur valide qui se trouve dans mon �num�ration de type Mois
			avion.moisMiseEnService = Mois::JANVIER;
		}
		else if (moisMiseEnServiceSaisie == Mois::FEVRIER) {
			avion.moisMiseEnService = Mois::FEVRIER;
		}
		else if (moisMiseEnServiceSaisie == Mois::MARS) {
			avion.moisMiseEnService = Mois::MARS;
		}
		else if (moisMiseEnServiceSaisie == Mois::AVRIL) {
			avion.moisMiseEnService = Mois::AVRIL;
		}
		else if (moisMiseEnServiceSaisie == Mois::MAI) {
			avion.moisMiseEnService = Mois::MAI;
		}
		else if (moisMiseEnServiceSaisie == Mois::JUIN) {
			avion.moisMiseEnService = Mois::JUIN;
		}
		else if (moisMiseEnServiceSaisie == Mois::JUILLET) {
			avion.moisMiseEnService = Mois::JUILLET;
		}
		else if (moisMiseEnServiceSaisie == Mois::AOUT) {
			avion.moisMiseEnService = Mois::AOUT;
		}
		else if (moisMiseEnServiceSaisie == Mois::SEPTEMBRE) {
			avion.moisMiseEnService = Mois::SEPTEMBRE;
		}
		else if (moisMiseEnServiceSaisie == Mois::OCTOBRE) {
			avion.moisMiseEnService = Mois::OCTOBRE;
		}
		else if (moisMiseEnServiceSaisie == Mois::NOVEMBRE) {
			avion.moisMiseEnService = Mois::NOVEMBRE;
		}
		else if (moisMiseEnServiceSaisie == Mois::DECEMBRE) {
			avion.moisMiseEnService = Mois::DECEMBRE;
		}

		


		cout << "Information de l'avion : " << endl;
		cout << "La capacit� en litre du r�servoir : " << avion.capaciteLitreReservoir << endl;
		cout << "Le nombre de places assises : " << avion.nombrePlacesAssises << endl;
		cout << "La date de mise en service : " << avion.jourMiseEnService << "/" << avion.moisMiseEnService << "/" << avion.anneeMiseEnService << endl;

		cout << "Voulez-vous saisir un nouvel avion? (o)ui ou (n)on : ";
		cin >> reponse;
	} while (reponse == 'o' || reponse == 'O');

	cout << "Le nombre de places assises total est de : " << accumulationPlace << endl;


	/*
	//D�claration de constante pour la gestion des mois. Est remplac� par une �numration
	const short JANVIER = 1;
	const short FEVRIER = 2;
	const short MARS = 3;
	const short AVRIL = 4;
	const short MAI = 5;
	const short JUIN = 6;
	const short JUILLET = 7;
	const short AOUT = 8;
	const short SEPTEMBRE = 9;
	const short OCTOBRE = 10;
	const short NOVEMBRE = 11;
	const short DECEMBRE = 12;
	*/
	/*
	//syntaxe de d�claration de variable : type nomVariable;
	short jour;
	short moisSaisie;
	short annee;

	Mois mois; //type : Mois, nomVariable : mois

	//mois = 1; // cette instruction est invalide car on ne peut pas assigner de valeur num�rique � une variable de type enum

	mois = Mois::AOUT; // Dans une variable de type �num�ration, on peut seulement placer des valeurs qui proviennent de cette �num�ration.

	//D�claration avec assignation de valeur imm�diatement
	Mois moisDeNaissance = Mois::MAI;

	cout << "Saisir le jour, le mois de l'ann�e d�sir� (1 = JANVIER, 2 = F�vrier... 12 = D�cembre) et l'ann�e : ";
	cin >> jour >> moisSaisie >> annee;


	cout << "Jour : " << jour << endl;
	cout << "Mois : ";
	if (moisSaisie == JANVIER) {
		cout << "janvier" << endl;
	}
	else if (moisSaisie == FEVRIER) {
		cout << "f�vrier" << endl;
	}
	else if (moisSaisie == MARS) {
		cout << "mars" << endl;
	}
	else if (moisSaisie == AVRIL) {
		cout << "avril" << endl;
	}
	else if (moisSaisie == MAI) {
		cout << "mai" << endl;
	}
	else if (moisSaisie == JUIN) {
		cout << "juin" << endl;
	}
	else if (moisSaisie == JUILLET) {
		cout << "juillet" << endl;
	}
	else if (moisSaisie == AOUT) {
		cout << "aout" << endl;
	}
	else if (moisSaisie == SEPTEMBRE) {
		cout << "septembre" << endl;
	}
	else if (moisSaisie == OCTOBRE) {
		cout << "octobre" << endl;
	}
	else if (moisSaisie == NOVEMBRE) {
		cout << "novembre" << endl;
	}
	else if (moisSaisie == DECEMBRE) {
		cout << "d�cembre" << endl;
	}
	cout << "Ann�e : " << annee << endl;
	*/

	system("pause");
	return 0;
}