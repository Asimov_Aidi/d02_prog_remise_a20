#include <iostream>
using namespace std;

int main() {
	bool reponse;
	cout << "Voulez-vous afficher un message? (0 = non, 1 = oui) : ";
	cin >> reponse;
	while (cin.fail()) {
		cin.clear(); // l�objet cin quitte le mode �chec
		cin.ignore(512, '\n'); // vider le tampon clavier des caract�res fautifs
		cout << "Attention - seulement 0 ou 1 : ";
		cin >> reponse;
	}
	if (reponse == true) {
		cout << "Voici le message!!!" << endl;
	}
	system("pause");
	return 0;
}
