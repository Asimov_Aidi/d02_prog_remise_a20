#include <iostream>
using namespace std;

enum Categorie {
	ASSURANCES,
	DIVERS,
	HABILLEMENT,
	LOISIRS,
	LOYER,
	NOURRITURE,
	TRANSPORT
};

int main() {
	double montantDepense;
	int typeSaisie;
	double accumalateurAssurance = 0;
	double accumalateurDivers = 0;
	double accumalateurHabillement = 0;
	double accumalateurLoisirs = 0;
	double accumalateurLoyer = 0;
	double accumalateurNourriture = 0;
	double accumalateurTransport = 0;

	do
	{
		cout << "Quel est la catégorie de dépense? " << endl;
		cin >> typeSaisie;
		cout << "Quel est le montant de la dépense? " << endl;
		cin >> montantDepense;
		if (typeSaisie == Categorie::ASSURANCES)
		{
			accumalateurAssurance = accumalateurAssurance + montantDepense;
		}
		else if (typeSaisie == Categorie::DIVERS)
		{
			accumalateurDivers = accumalateurDivers + montantDepense;
		}
		else if (typeSaisie == Categorie::HABILLEMENT)
		{
			accumalateurHabillement = accumalateurHabillement + montantDepense;
		}
		else if (typeSaisie == Categorie::LOISIRS)
		{
			accumalateurLoisirs = accumalateurLoisirs + montantDepense;
		}
		else if (typeSaisie == Categorie::LOYER)
		{
			accumalateurLoyer = accumalateurLoyer + montantDepense;
		}
		else if (typeSaisie == Categorie::NOURRITURE)
		{
			accumalateurNourriture = accumalateurNourriture + montantDepense;
		}
		else if (typeSaisie == Categorie::TRANSPORT)
		{
			accumalateurTransport = accumalateurTransport + montantDepense;
		}
	} while (typeSaisie != -1);
	cout << "Total catégorie assurance: " << accumalateurAssurance << endl;
	cout << "Total catégorie divers: " << accumalateurDivers << endl;
	cout << "Total catégorie habillement: " << accumalateurHabillement << endl;
	cout << "Total catégorie loisirs: " << accumalateurLoisirs << endl;
	cout << "Total catégorie loyer: " << accumalateurLoyer << endl;
	cout << "Total catégorie nourriture: " << accumalateurNourriture << endl;
	cout << "Total catégorie transport: " << accumalateurTransport << endl;
	cout << "Total Toute catégorie: " << accumalateurTransport + accumalateurNourriture + accumalateurLoyer + accumalateurLoisirs + accumalateurAssurance + accumalateurHabillement + accumalateurDivers << endl;
	system("pause");
	return 0;
}