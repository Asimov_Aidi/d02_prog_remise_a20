//1) tableau de valeur enti�re a 12 case
#include <iostream>
using namespace std;

int main() {
	int  tabEntier[12] = { 1, 4, 7, 10, 0 , 34 };
	for (int i = 0; i < 12; i++) {
		cout << i << " = " << tabEntier[i] << endl;
	}
	system("pause");
	return 0;
}

//2)
#include <iostream>
using namespace std;

int main() {
	int  direction[4] = { 'N', 'S', 'E', 'O' };
	for (int i = 0; i < 4; i++) {
		cout << i << " = " << direction[i] << endl;
	}
	system("pause");
	return 0;
}

//3)
#include <iostream>
using namespace std;

int main() {
	int  VALEURS_CONSTANTES[6] = { 0.005,  -0.032,  0.000001,  0.167,  -100000.3,  0.015 };
	for (int i = 0; i < 6; i++) {
		cout << i << " = " << VALEURS_CONSTANTES[i] << endl;
	}
	system("pause");
	return 0;
}

//4)
#include <iostream>
using namespace std;

int main() {
	int  tab[10] = {};
	for (int i = 0; i < 10; i++) {
		cin >>tab[i];
	}
	system("pause");
	return 0;
}