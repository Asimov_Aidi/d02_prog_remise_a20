/*
SousTotal = 0
	RÉPÉTER
		ECRIRE "Quel est le poids de la lettre? (en gramme)(0 pour arrêter)"
		LIRE Poids
		SI Poids = 0 ALORS
			MontantLettre = 0
		SINON SI Poids < 27 ALORS
			MontantLettre = 0.45
		SINON SI Poids < 81 ALORS
			MontantLettre = 0.90
		SINON SI Poids < 120  ALORS
			MontantLettre = 1.10
		SINON SI Poids > 120 ALORS
			MontantLettre = 1.50
		FINSI
		SousTotal = SousTotal + MontantLettre
	TANTQUE Poids != 0
	ECRIRE "Votre sous-total est de: ", SousTotal
	SI SousTotal >= 20 ALORS
		MontantRéduction = SousTotal * Rabais
	SINON
		MontantRéduction = 0
	FINSI
	ECRIRE "Votre réduction est de ", MontantRéduction
	MontantTaxe = SousTotal * Taxe
	ECRIRE "Les taxes sont de ", MontantTaxe
	Total = (SousTotal + MontantTaxe) - MontantRéduction
	ECRIRE "Votre montant total est de ", Total
*/

#include <iostream>
using namespace std;

int main() {
	double Poids;
	double MontantLettre;
	double SousTotal;
	double MontantRéduction;
	double MontantTaxe;
	double Total;
	const double Taxe = 0.15;
	const double Rabais = 0.10;

	SousTotal = 0;

	do
	{
		cout << "Quel est le poids de la lettre? (en gramme)(0 pour arrêter)";
		cin >> Poids;
		if (Poids == 0){
			MontantLettre = 0;
		} 
		else if (Poids < 27)
		{
			MontantLettre = 0.45;
		}
		else if (Poids < 81)
		{
			MontantLettre = 0.90;
		}
		else if (Poids < 120)
		{
			MontantLettre = 1.10;
		}
		else if (Poids > 120)
		{
			MontantLettre = 1.50;
		}
		SousTotal = SousTotal + MontantLettre;
	} while (Poids != 0);
	cout << "Votre sous-total est de: " << SousTotal << endl;
	if (SousTotal >= 20)
	{
		MontantRéduction = SousTotal * Rabais;
	}
	else
	{
		MontantRéduction = 0;
	}
	cout<< "Votre réduction est de "<< MontantRéduction << endl;
	MontantTaxe = SousTotal * Taxe;
	cout << "Les taxes sont de " << MontantTaxe << endl;
	Total = (SousTotal + MontantTaxe) - MontantRéduction;
	cout << "Votre montant total est de" << Total << endl;

	system("pause");
	return 0;
}