/* analyse
entr�
	ageJoueur
Sortie
	moyenneEquipe
	moyenneLigue
Donn�e interne
	NombreEQ = 3
	NombreJoueur = 5
Traitement
	Demander en boucle l'age d'un joueur pour une EQ (stocker en accumulateur et diviser par 5 pour la moyenne)
	Demander en boucle la ligne pr�c�dente pour le nombre d'�quipe (stocker les moyenne en accumulateur et diviser par 3 pour la moyenne)
*/

#include <iostream>
using namespace std;

int main() {
	int ageJoueur;
	double moyenneEquipe;
	double moyenneLigue;
	const int nombreEQ = 3;
	const int nombreJoueur = 5;


	moyenneEquipe = 0;
	moyenneLigue = 0;
	for (int i = 0; i < nombreEQ; i++){
		moyenneEquipe = 0;
		for (int j = 0; j < nombreJoueur; j++){
			cout << "Quel est l'age du joueur?" << endl;
			cin >> ageJoueur;
			while (ageJoueur < 18 || ageJoueur > 65){
				cout << "Age invalide, doit �tre entre 18 et 65" << endl;
				cin >> ageJoueur;
			}
			moyenneEquipe = moyenneEquipe + ageJoueur;
		}
		moyenneEquipe = moyenneEquipe / nombreJoueur;
		cout << "La moyenne de l'�quipe est de : " << moyenneEquipe << endl;
		moyenneLigue = moyenneLigue + moyenneEquipe;
	}
	moyenneLigue = moyenneLigue / nombreEQ;
	cout << "La moyenne de la ligue est de : " << moyenneLigue << endl;

	

	system("pause");
	return 0;
}